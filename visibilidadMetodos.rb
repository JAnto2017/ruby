#Formas de establecer la visibilidad: public, protected, private

class MiClase

    def metodo_uno
    end

    def metodo_dos
    end

    def metodo_tres
    end

    #visibilidad de los metodos
    public :metodo_uno
    protected :metodo_dos
    private :metodo_tres
end

#Clase Animal, para probar la herencia
class Animal
    attr_accessor :nombre, :nombre_cientifico, :sexo

    def initialize
        puts "Animal"
    end

    def dormir(cantidad_de_hrs)
        puts "Me despierto en #{cantidad_de_hrs} hrs"
    end

    def comer(alimento)
        puts "Comiendo #{alimento}"
    end
end

#Clase que contiene tres atributos. Que hereda de la clase Animal

class Vertegrado < Animal
    attr_accessor :tipo_de_fecundacion, :temperatura, :tipo_de_respiracion

    def initialize
        puts "Vertegrado"
    end

    animal_vertebrado = Vertegrado.new
    animal_vertebrado.nombre = "Chango"
end
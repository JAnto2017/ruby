#Clase Animal, para probar la herencia
class Animal
    attr_accessor :nombre, :nombre_cientifico, :sexo

    def initialize
        puts "Animal"
    end

    def dormir(cantidad_de_hrs)
        puts "Me despierto en #{cantidad_de_hrs} hrs"
    end

    def comer(alimento)
        puts "Comiendo #{alimento}"
    end
end
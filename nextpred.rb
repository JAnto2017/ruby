#El método next devuelve el nº siguiente.
#El método pred devuelve el nº anterior.

numero = 99
puts numero
puts numero.next
puts numero.pred

#----------------------------------------------

num_binario = 0b1001
puts num_binario,' nº binario'
puts num_binario.next
puts num_binario.pred

#Sistemas de numeración en Ruby
#nº grandes
puts 12_345_628_895_234
#nº binarios
puts '0b101',0b101
puts 0b1000
puts 0b1111110
#sistema octal
puts 0o1234567
puts 0o127
puts 0o272
#nº hexadecimal
puts 0xFF0000
puts 0x00FF00
puts 0x0000FF
#una miscelánea de métodos básicos

animales = ["liebre","perro","gato","ratón","iguana","loro","tortuga"]

print animales.first
print animales.first(3)
print animales.last
print animales.last(2)

#----------------------------------------------------------------------------

num_variados = [-1,23,-6,7,9,43,-58,82,-13,61,35,0,0]

puts num_variados.length
puts num_variados.size
puts num_variados.count
puts "Cantidad de ceros",num_variados.count(0)

#-----------------------------------------------------------------------------

estaturas = [1.78,1.70,1.55,1.82,1.67,1.68,1.70,1.85]

print estaturas.sort
print estaturas.reverse
print estaturas.sort.reverse

#------------------------------------------------------------------------------

calificaciones = [74,76,75,81,100,70,87]

puts calificaciones.min
puts calificaciones.max
puts calificaciones.sum
puts calificaciones.length
puts calificaciones.reduce(:+)
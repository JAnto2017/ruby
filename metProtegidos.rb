#Métodos que pueden ser accedidos dentro de sus subclases

class A
    protected

    def metodo_protegido
        puts "Método protegido"
    end
end

class B < A
    def metodo_b
        metodo_protegido
    end
end

b = B.new
b.metodo_b
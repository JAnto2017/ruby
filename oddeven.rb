#Para saber si nº es par o impar
#métodos odd? => devuelve True si nº es impar
#método even? => es lo opuesto

_23, _32 = 23, 32

puts 'Nº 23'
puts _23.odd?
puts _23.even?

puts '-----------------'
puts 'Nº 32'
puts _32.odd?
puts _32.even?
